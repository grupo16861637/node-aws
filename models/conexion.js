import mysql from 'mysql2'

var conexion = mysql.createConnection({
    host: '127.0.0.1',
    user: 'root',
    password:'cisco123',
    database: 'sistemas'
});

conexion.connect(function(err){
    if(err){
        console.error("Ocurrió un error al conectar a la base de datos:");
        console.error(err); // Imprime el objeto de error completo
   }else{
        console.log("Conexion Exitosa")
    }
});

export default conexion;