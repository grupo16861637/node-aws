import conexion from './conexion.js';
import {rejects} from "assert";
import { resolve } from 'path'; // Renombrar para evitar conflicto de nombres

var alumnosDb ={};

alumnosDb.insertar = function insertar (alumno){

    return new Promise((resolve, rejects)=>{

        let sqlConsulta = 'Insert into alumnos set ?';
            conexion.query(sqlConsulta,alumno,function(err,res){
            if(err){
                console.error('Surgio un error', err.message);
                rejects(err);
            }else{
             const alumno ={
                 id: res.insertId,
            }
            resolve(alumno);
            }
        })

    }
)}

alumnosDb.mostrartodos = function mostrartodos (alumno){

    return new Promise((resolve, rejects)=>{

        let sqlConsulta = 'select * from alumnos';
            conexion.query(sqlConsulta,null,function(err,res){
            if(err){
                console.error('Surgio un error', err.message);
                rejects(err);
            }else{
            resolve(res);
            }
        })

    }
)}



export default alumnosDb;